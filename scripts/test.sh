#!/bin/sh
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 180x150+0+20 -trim /home/stefano/assetstwa/source/equipment/chest.png 
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x150+200-10 -trim /home/stefano/assetstwa/source/equipment/shoulder-left.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x150+360-20 -trim /home/stefano/assetstwa/source/equipment/shoulder-right.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x150+200+150 -trim /home/stefano/assetstwa/source/equipment/arm-left.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x150+360+150 -trim /home/stefano/assetstwa/source/equipment/arm-right.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 180x150+0+250 -trim /home/stefano/assetstwa/source/equipment/pelvis.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 180x150+200+250 -trim /home/stefano/assetstwa/source/equipment/glove-left.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x100+0+400 -trim /home/stefano/assetstwa/source/equipment/glove-right.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 150x100+100+400 -trim /home/stefano/assetstwa/source/equipment/leg.png
#convert /home/stefano/assetstwa/source/equipment/ArmorsSets/AssassinArmor.png -crop 180x150+200+400 -trim /home/stefano/assetstwa/source/equipment/boot.png


for filename in /home/stefano/assetstwa/source/equipment/ArmorsSets/*.png; 
do
    DIR="${filename%.*}/"
    mkdir -p $DIR
    convert "$filename" -crop 180x150+0+20 "$DIR"chest.png 
    convert "$filename" -crop 150x150+200-10 "$DIR"shoulder-left.png
    convert "$filename" -crop 150x150+360-20 "$DIR"shoulder-right.png
    convert "$filename" -crop 150x150+200+150 "$DIR"arm-left.png
    convert "$filename" -crop 150x150+360+150 "$DIR"arm-right.png
    convert "$filename" -crop 180x150+0+250 "$DIR"pelvis.png
    convert "$filename" -crop 180x150+200+250 "$DIR"glove-left.png
    convert "$filename" -crop 150x100+0+400 "$DIR"glove-right.png
    convert "$filename" -crop 150x100+100+400 "$DIR"leg.png
    convert "$filename" -crop 180x150+200+400 "$DIR"boot.png
    #echo "$DIR"
done
for filename in /home/stefano/assetstwa/source/equipment/ArmorsSets/**/*.png; do
    echo "$filename"
    convert "$filename" -trim "$filename" 
done

#for filename in /home/stefano/assetstwa/equipment/inventory/*/*.png; do
#for filename in /home/stefano/assetstwa/**/*.png; do
    #convert "$filename" -trim "$filename" 
#done